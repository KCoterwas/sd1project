public class BreadCrumbTrail {
	BreadCrumb first;
	BreadCrumb last;
	
	private class BreadCrumb {
		public Location loc;
		private BreadCrumb next;
		
		public BreadCrumb (Location l, BreadCrumb b) {
			loc = l;
			next = b;
		}
	}
	
	public BreadCrumbTrail () {
		first = null;
		last = null;
	}
	
	public boolean isEmpty () {
		return first == null;
	}
	
	public int size () {
		int i = 0;
		BreadCrumb b = first;
		while (b != null) {
			i++;
			b = b.next;
		}
		return i;
	}
	
	public void add(Location loc) {
		last = first;
		first = new BreadCrumb(loc, last);
	}
	
	public Location remove() {
		Location loc = null;
		if (size() > 0) {
			loc = first.loc;
			first = last;
		}
		if (size() > 1) {
			last = last.next;
		} else {
			last = null;
		}
		return loc;
	}
}
