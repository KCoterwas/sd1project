
import java.util.Scanner;

public class GameEngine {
	private boolean isRunning;
	protected static String output;
	private static int moves;
	protected Player player;
	private World world;
	
	public static void main(String[] args) {
		GameEngine engine = new GameEngine();
		Scanner scanner = new Scanner(System.in);
		String input;
		
		engine.intro();
		
		scanner.nextLine();	
		engine.render();
		
		while (engine.isRunning) {
			System.out.print("Enter action: ");
			input = scanner.nextLine();
			if (!input.isEmpty()) {
				engine.read(input);
			}
		}
		engine.exit();
	}
	
	public GameEngine () {
		world = new World();
		player = new Player("Kim", world.getLoc(0), false, world);
		output = "";
		moves = 0;
		isRunning = true;
	}
	
	//reads user's input
	public void read(String input) {
		Scanner scanner = new Scanner(input);
		String action = scanner.next();
		action = action.toLowerCase();
		String object = "";
		if (scanner.hasNextLine()) {
			object = scanner.nextLine();
			object = object.trim().toLowerCase();
		}
		
		update(action, object);
	}
	
	//updates game's options
	public void update(String action, String object) {
		output = "";
		
		if (action.equals("q") || action.equals("quit")) {
			isRunning = false;
		} else if (action.equals("h")) {
			GameEngine.updateOutput("Help");
			GameEngine.updateOutput("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
			GameEngine.updateOutput("'l' or 'look': Look around");
			GameEngine.updateOutput("n: Move North");
			GameEngine.updateOutput("s: Move South");
			GameEngine.updateOutput("e: Move East");
			GameEngine.updateOutput("w: Move West");
			GameEngine.updateOutput("'t [item]' or 'take [item]': Take an item");
			GameEngine.updateOutput("'d [item]' or 'drop [item]': Drop an item");
			GameEngine.updateOutput("'i' or 'inventory': Check Inventory");
			GameEngine.updateOutput("'m' or 'map': Check Map");
			GameEngine.updateOutput("'b': Backtrack");
			GameEngine.updateOutput("'q' or 'quit': Quit");
		} else if (action.equals("l") || action.equals("look")) {
			player.look();
		} else if (action.equals("n")) {
			moves++;
			player.move(Movement.NORTH);
		} else if (action.equals("s")) {
			moves++;
			player.move(Movement.SOUTH);
		} else if (action.equals("e")) {
			moves++;
			player.move(Movement.EAST);
		} else if (action.equals("w")) {
			moves++;
			player.move(Movement.WEST);
		} else if (action.equals("t") || action.equals("take")) {
				player.take(object);
		} else if (action.equals("d") || action.equals("drop")) {
				player.drop(object);
		} else if (action.equals("i") || action.equals("inventory") || action.equals("check inventory")) {
			player.chkInv();
		} else if (action.equals("m") || action.equals("map") || action.equals("check map")){
			player.chkMap();
		} else if (action.equals("b")) {
			if (object.isEmpty()) {
				player.backtrack();
			} else {
				player.backtrack(object);
			} 
		} else {
			GameEngine.updateOutput("Invalid command. Please press 'h' for help.");
		}
		
		
		render();
	}
	
	//updates game's output
	public void render() {
		System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
		System.out.println();
		System.out.println("# of Moves: " + moves);
		System.out.println("Directions: " + getDir());
		System.out.println();
		player.getLoc().printDesc();
		System.out.println();
		if (!output.isEmpty()) {
			System.out.println(output);
		}
	}
	
	public String getDir() {
		String str = "";
		int iLoc = world.getLocIndex(player.getLoc());
		if (iLoc != -1) {
			if (world.dirExists(iLoc, Movement.NORTH)) {
				str += "N ";
			} else {
				str += "_ ";
			}
			if (world.dirExists(iLoc, Movement.EAST)) {
				str += "E ";
			} else {
				str += "_ ";
			}
			if (world.dirExists(iLoc, Movement.SOUTH)) {
				str += "S ";
			} else {
				str += "_ ";
			}
			if (world.dirExists(iLoc, Movement.WEST)) {
				str += "W ";
			} else {
				str += "_ ";
			}
		} else {
			System.err.println("Invalid player location.");
		}
		return str;
	}
	
	//prints game intro
	public void intro() {
		System.out.println("Welcome to my game! This game will test your sanity has you type in commands to pick items, \nand then, prepare yourself, PUT THEM DOWN. Sometimes, you'll even be able to move\nto different locations.");
		System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
		System.out.println("Press enter to begin...");
	}
	
	//prints game outro
	public void exit() {
		System.out.println("Thanks for playing!");
		System.out.println("Completed for Software Development I");
		System.out.println("Made by Kim Coterwas");
		System.out.println("Copyright -*- April 24, 2015");
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public static void updateOutput(String str) {
		output += str + "\n";
	}
	
}