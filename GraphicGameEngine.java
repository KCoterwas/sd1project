import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
 



import java.awt.BorderLayout;

import javax.swing.JOptionPane;
 



import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GraphicGameEngine extends GameEngine{
	private JFrame frame;
	private JLabel lblDesc, lblMoves, lblInv;
	private JTextField txtInput;
	
	public JPanel buildGUI(){
		lblDesc = new JLabel("desc");
		lblDesc.setHorizontalAlignment(JLabel.CENTER);
		lblDesc.setVerticalAlignment(JLabel.CENTER);
		
		lblMoves = new JLabel("Steps Taken: ");
		lblMoves.setHorizontalAlignment(JLabel.CENTER);
		lblMoves.setVerticalAlignment(JLabel.CENTER);
		
		lblInv = new JLabel("Inventory: ");
		lblInv.setHorizontalAlignment(JLabel.CENTER);
		lblInv.setVerticalAlignment(JLabel.CENTER);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JLabel("<html><h1>Text Adventure</h1></html>", JLabel.CENTER), BorderLayout.PAGE_START);
		panel.add(lblDesc, BorderLayout.CENTER);
		panel.add(lblMoves, BorderLayout.LINE_START);
		panel.add(lblInv, BorderLayout.LINE_END);
		panel.add(buildInputGUI(), BorderLayout.PAGE_END);
		intro();
		return panel;
	}
	
	//receives user's input from getInput()
	public void step(){
		lblDesc.setText(getInput());
		read(getInput());
	}
	
	//retrieves user's input 
	public String getInput() {
		if(!txtInput.getText().isEmpty()){
			return txtInput.getText();
		}else{
			return "";
		}
	}
	
	//prints the panel of the game
	public GraphicGameEngine() {
		super();
		
		GameEngine eng = this;
		frame = new JFrame("Adventure");
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				eng.exit();
			}
		});
		frame.getContentPane().setPreferredSize(new Dimension(600, 200));
		frame.add(buildGUI());
		frame.pack();
	}
	
	public static void main (String[] args){
		GraphicGameEngine engine = new GraphicGameEngine();
		engine.frame.setVisible(true);
		engine.render();
		
	}
	
	//
	public void render(){
		lblDesc.setText("<html><body>" + player.getLoc().toString() + "</body></html>");
		lblMoves.setText("<html><body>" + "Steps taken: " + player.getSteps() + "</body></html>");
		lblInv.setText("<html><body>" + "Inventory: " + player.invTransform() + "</body></html>");;
		if (!output.isEmpty()){
			JOptionPane.showMessageDialog(frame, output);
		}
	}
	
	public JPanel buildInputGUI(){
		GraphicGameEngine eng = this;
		
		JPanel panel = new JPanel();
		panel.add(new JLabel("Enter an action: "));
		txtInput = new JTextField(8);
		panel.add(txtInput);
		txtInput.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				eng.step();
				((JTextField)e.getSource()).setText(null);
			}
		});
		JButton bttnHelp = new JButton("HALP ME");
		bttnHelp.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				eng.help();
			}
		});
		panel.add(bttnHelp);
		return panel;
	}
	
	public void help(){
		String helpme = "Help";
		helpme += "\n" + "*-*-*-*-*-*-*-*-*-*-*-*-*-*";
		helpme += "\n" + "'l' or 'look': Look around";
		helpme += "\n" + "'n': Move North";
		helpme += "\n" + "'s': Move South";
		helpme += "\n" + "'e': Move East";
		helpme += "\n" + "'w': Move West";
		helpme += "\n" + "'t [item]' or 'take [item]': Take an item";
		helpme += "\n" + "'d [item]' or 'drop [item]': Drop an item";
		helpme += "\n" + "'i' or 'inventory': Check Inventory";
		helpme += "\n" + "'m' or 'map': Check Map";
		helpme += "\n" + "'b': Backtrack";
		JOptionPane.showMessageDialog(frame, helpme);
	}
	
	public void intro(){
		String intro = "Welcome to my game! This game will test your sanity has you type in commands to pick items, \nand then, prepare yourself, PUT THEM DOWN. Sometimes, you'll even be able to move\nto different locations.";
		intro += "\n" + "*-*-*-*-*-*-*-*-*-*-*-*-*-*";
		intro += "\n" + "Press enter to begin...";
		JOptionPane.showMessageDialog(frame, intro);
	}
	
	public void exit(){
		String outro = "Thanks for playing!";
		outro += "\n" + "Completed for Software Development I";
		outro += "\n" + "Made by Kim Coterwas";
		outro += "\n" + "Copyright -*- April 24, 2015";
		JOptionPane.showMessageDialog(frame, outro);
	}
}
