
public class Item {
	//map of world
	//3 more items
	private String name, description;
	
	public Item(String name, String description){
		this.name = name;
		this.description = description;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
}
