import java.util.ArrayList;

public class Location {
	//name, items, and description
	private String name, description;
	private ArrayList<Item> items;
	
	public Location (String name, String description){
		this.name = name;
		this.description = description;
		items = new ArrayList<Item>(0);
	}
	
	public Location (String name, String description, Item[] items){
		this.name = name;
		this.description = description;
		this.items = new ArrayList<Item>(items.length);
		for(int i = 0; i < items.length; i++){
			this.items.add(items[i]);
		}
	}
	
	
	public String getName() {
		return name;
	}
	
	public void printDesc() {
		System.out.println(description);
		if (items.size() > 2) {
			String str = "";
			for (int i = 0; i < items.size() - 1; i++) {
				str += items.get(i).getName() + ", ";
			}
			str += "and " + items.get(items.size() - 1).getName();
			System.out.println("There is a " + str + " here.");
		} else if (items.size() > 1) {
			System.out.println("There is a " + items.get(0).getName() + " and " + items.get(1).getName() + " here.");
		} else if (items.size() > 0) {
			System.out.println("There is a " + items.get(0).getName() + " here.");
		}
	}
	
	public String toString(){
		String result = description;
		if(items.size() > 2) {
			String str = "";
			for(int i = 0; i < items.size() - 1; i++) {
				str += items.get(i).getName() + ", ";
			}
			str += "and " + items.get(items.size() - 1).getName();
			result += "\nOThere is a " + items.get(0).getName() + ".";
		}else if (items.size() > 1){
			result += "\nThere is a " + items.get(0).getName() + " and " + items.get(1).getName() + ".";
		}else if (items.size() > 0){
			result += "\nThere is a " + items.get(0).getName() + ".";
		}
		return result;
	}
	
	public int indexOfItem(String item) {
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).getName().equals(item)) {
				return i;
			}
		}
		return -1;
	}
	
	public void addItem(Item item) {
		items.add(item);
	}
	
	public void removeItem(int i) {
		items.remove(i);
	}
	
	public Item getItem(int i) {
		return items.get(i);
	}
	
}
