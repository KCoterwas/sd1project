
public enum Movement {
	NORTH(0), EAST(1), SOUTH(2), WEST(3);
	public int index;
	
	private Movement(int val) {
		this.index = val;
	}
	
	public Movement opposite() {
		switch (this) {
			case NORTH: return SOUTH;
			case EAST: return WEST;
			case SOUTH: return NORTH;
			case WEST: return EAST;
			default: return null;
		}
	}
}
