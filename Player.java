import java.util.Scanner;
import java.util.ArrayList;

public class Player {
	private String name;
	private boolean male;
	private ArrayList<Item> inventory;
	private Location loc;
	private BreadCrumbTrail bctrail;
	private World world;
	
	public Player (String name) {
		this.name = name;
		this.inventory = new ArrayList<Item>(0);
	}
	
	public Player (String name, Location loc, int invSize) {
		this.name = name;
		this.loc = loc;
		this.inventory = new ArrayList<Item>(invSize);
	}
	
	public Player (String name, Location loc, boolean male, World world) {
		this.name = name;
		this.male = male;
		this.loc = loc;
		this.world = world;
		this.inventory = new ArrayList<Item>(0);
		bctrail = new BreadCrumbTrail();
	}
	
	public void look() {
		loc.printDesc();
	}
	
	public void take () {
		System.out.println();
		Scanner scanner = new Scanner(System.in);
		System.out.println("What would you like to take?");
		String input = scanner.nextLine();
		input = input.trim().toLowerCase();
		take(input);
	}
	
	public void take (String item) {
		int thing = loc.indexOfItem(item);
		if (thing != -1) {
			inventory.add(loc.getItem(thing));
			loc.removeItem(thing);
			GameEngine.updateOutput("You picked up " + item + ".");
		} else {
			GameEngine.updateOutput("There is no " + item + " here.");
		}
	}
	
	public void drop () {
		System.out.println();
		Scanner scanner = new Scanner(System.in);
		System.out.println("What would you like to drop?");
		String input = scanner.nextLine();
		input = input.trim().toLowerCase();
		drop(input);
	}
	
	public void drop (String item) {
		int thing = indexOfItem(item);
		if (thing != -1) {
			loc.addItem(inventory.get(thing));
			inventory.remove(thing);
			GameEngine.updateOutput("You dropped " + item + ".");
		} else {
			GameEngine.updateOutput("You do not have " + item + ".");
		}
	}
	
	public void chkInv () {
		if (inventory.size() > 2) {
			String str = "";
			for (int i = 0; i < inventory.size() - 1; i++) {
				str += inventory.get(i).getName() + ", ";
			}
			str += "and " + inventory.get(inventory.size() - 1).getName();
			GameEngine.updateOutput("You have a " + str + " in your inventory.");
		} else if (inventory.size() > 1) {
			GameEngine.updateOutput("You have a " + inventory.get(0).getName() + " and " + inventory.get(1).getName() + " in your inventory.");
		}else if (inventory.size() > 0) {
			GameEngine.updateOutput("You have a " + inventory.get(0).getName() + " in your inventory.");
		} else {
			GameEngine.updateOutput("You have nothing in your inventory.");
		}
	}
	
	public String invTransform(){
		String str = "";
		for(int i = 0; i < inventory.size(); i++){
			str += "<p>" +inventory.get(i).getName() + "</p>";
		}
		return str;
	}
	
	public void chkMap () {
		if (hasMap()) {
			GameEngine.updateOutput("      River === McDonald's === Forest\n                  ||             ||\n        Maze === Field === Three Stones === Cathedral\n                  ||\n                Gypsy Cart");
		} else {
			GameEngine.updateOutput("You don't have a map.");
		}
	}
	
	public boolean hasMap() {
		boolean result = false;
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i).getName() == "map") {
				result = true;
			}
		}
		return result;
	}
	
	public int indexOfItem(String item) {
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i).getName().equals(item)) {
				return i;
			}
		}
		return -1;
	}
	
	public static boolean tryParseInt(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	
	public void move(Movement dir) {
		int iLoc = world.getLocIndex(loc);
		if (iLoc != -1) {
			if (world.dirExists(iLoc, dir)) {
				bctrail.add(loc);
				loc = world.getLocTo(iLoc, dir);
			} else {
				GameEngine.updateOutput("There is no path in that direction.");
			}
		} else {
			System.err.println("ERROR: Player location not recognized.");
		}
	}
	
	public void backtrack () {
		if (!bctrail.isEmpty()) {
			loc = bctrail.remove();
		} else {
			GameEngine.updateOutput("You're at the beginning of your trail.");
		}
	}

	public void backtrack (String input) {
		if (tryParseInt(input)) {
			int count = Integer.parseInt(input);
			for (int i = 0; i < count; i++) {
				if (!bctrail.isEmpty()) {
					backtrack();
				} else {
					GameEngine.updateOutput("You're at the beginning of your trail.");
					break;
				}
			}
		} else {
			backtrack();
		}
	}
	
	public Location getLoc() {
		return loc;
	}
	
	public int getSteps() {
		return bctrail.size();
	}
}

