

public class World {
	//8 locations
	private int[][] connections;
	private Location[] locations;
	
	private static final int NOWHERE = -1;
	private static final int NORTH = 0;
	private static final int SOUTH = 1;
	private static final int EAST = 2;
	private static final int WEST = 3;
	
	public World(){
		locations = new Location[]{
				new Location("Forest", "You are in a opening in the middle of what appears to be a neverending forest.", new Item[]{
						new Item("bag of dirt", "Just an ordinary bag of dirt. Doesn't appear to be important."),
				}), 
				new Location("McDonald's", "You climb past the trees to see a McDonald's. Wow, they really are everywhere."),
				new Location("Stones", "You come upon three 8-foot tall rectangular stones in the ground. Each has its own unique markings."),
				new Location("Field", "You come into a clearing with signs pointing in each direction."),
				new Location("River", "You traverse over some rocks to discover a flowing river.", new Item[]{
						new Item("map", "A well-worn piece of paper that appears to show all of the local areas.")
				}), 
				new Location("Abandoned Gypsy Cart", "You arrive upon an abandoned gypsy cart.", new Item[]{
						new Item("shiny trinket", "A sparkling cube-shaped object with an illegible carving on the side of it.")
				}), 
				new Location("Cathedral", "You arrive in an extravagant cathedral with a sword stuck in a stone in the center of the single room.", new Item[]{
						new Item("sword", "A sharp weapon that oddly has your name engraved on the handle.")
				}), 
				new Location("Large Maze", "As you approach a large wall of foliage, you realize that it has an entrance.\nIt appears to be a giant maze. You don't feel ready to attempt this yet \n(neither does the developer).")
		};
		connections = new int[locations.length][4];
		for(int i = 0; i < locations.length; i++){
			connections[i][NORTH] = NOWHERE;
			connections[i][SOUTH] = NOWHERE;
			connections[i][EAST] = NOWHERE;
			connections[i][WEST] = NOWHERE;
		}
		connect(0, 1, Movement.WEST);
		connect(1, 4, Movement.WEST);
		connect(2, 3, Movement.WEST);
		connect(3, 7, Movement.WEST);
		connect(2, 6, Movement.EAST);
		connect(2, 0, Movement.NORTH);
		connect(3, 1, Movement.NORTH);
		connect(3, 5, Movement.SOUTH);
	}
	
	private void connect(int from, int to, Movement dir) {
		connections[from][dir.index] = to;
		connections[to][dir.opposite().index] = from;
	}
	
	public int getLocIndex(Location loc) {
		for (int i = 0; i < locations.length; i++) {
			if (locations[i] == loc) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean dirExists(int i, Movement dir) {
		if (connections[i][dir.index] != NOWHERE) {
			return true;
		} else {
			return false;
		}
	}
	
	public Location getLocTo(int loc, Movement dir) {
		if (dirExists(loc, dir)) {
			return locations[connections[loc][dir.index]];
		} else {
			return null;
		}
	}
	
	public Location getLoc(int i) {
		if (i < locations.length) {
			return locations[i];
		} else {
			return null;
		}
	}
}